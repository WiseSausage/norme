# README #

Projet ORM C# Bachelor 3 Ingesup
Participants:
 -ARSAC Guillaume
 -BEZANCON Pierre
 -BOULAND Lucas
 -GBAHA Patrick

### What is this repository for? ###

* ORM basique en C#
* Version 0.1

### How do I get set up? ###

V0.1:
1.Installer MySQL.
2.Remplacer les champs dans les connexions par ceux du serveur et utilisateur qui vont êtres utilisés.
3.Lancer les tests unitaires pour vérifier si l'ORM fonctionne ou non (spoiler: non).

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact